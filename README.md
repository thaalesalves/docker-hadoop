# Apache Hadoop for Docker
Imagem de Docker para execução do Apache Hadoop para execução de processos de Big Data. Veja no [Docker Hub](https://hub.docker.com/r/thaalesalves/hadoop).

## O que inclui?
- Apache Hadoop;
- Apache Hive;
- Apache Pig;
- Apache Sqoop;
- Cliente MySQL/MariaDB;
- Cliente PostgreSQL/Greenplum;
- Cliente git;
- Editores de texto (mcedit, emacs, nano).

## Requerimentos
Alguns tarballs foram incluídos para download, com seus arquivos de configuração alterados, mas alguns deles não podem ser redistribuidos por terceiros, portanto, devem ser baixados nas versões corretas e colocados na pasta `packages` com os nomes corretos manualmente.
1. JDK 8u181: [jdk-8u181-linux-x64.rpm](https://www.oracle.com/technetwork/java/javase/downloads/java-archive-javase8-2177648.html);
2. Driver OJDBC: [ojdbc6.jar](https://www.oracle.com/technetwork/documentation/jdbc-112010-090769.html).

## Como funciona?
**Nota:** é necessário que os containers de Greenplum, MySQL e Oracle estejam rodando e aceitando conexões em suas respectivas portas para que esta imagem seja executada. Caso os containers citados não estejam rodando, o container de hadoop entrará em loop até que encontre os demais rodando na mesma rede. Para instruções de como subir a imagem com docker-compose, veja a descição no [Docker Hub](https://hub.docker.com/r/thaalesalves/hadoop).
1. Clone o repositório: `git clone https://github.com/thaalesalves/docker-hadoop.git`;
2. Compile a imagem: `docker build -t NOME_DA_IMAGEM .` (Exemplo: `docker build -t thaalesalves/hadoop .`);
3.  Inicie a imagem num container: `docker run --hostname=hadoop --name=hadoop -itd SEU_USUARIO/NOME_ESCOLHIDO`;
4.  Acesse o container através do bash: `docker exec -it hadoop bash`.
