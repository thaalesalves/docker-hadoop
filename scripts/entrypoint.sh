#!/bin/bash

# Script central
localedef -c -f UTF-8 -i pt_BR pt_BR.UTF-8
echo 'export CLASSPATH=$CLASSPATH:/usr/local/hadoop/lib/*:$HIVE_HOME/lib/*:/usr/local/hadoop/lib/*:/usr/local/hive/lib/*:.'>>/etc/bashrc
echo 'export HADOOP_CLIENT_OPTS=" -Xmx2048m"'>>/etc/bashrc
echo 'export HADOOP_COMMON_HOME=/usr/local/hadoop'>>/etc/bashrc
echo 'export HADOOP_CONF_DIR=/usr/local/hadoop/etc/hadoop'>>/etc/bashrc
echo 'export HADOOP_HDFS_HOME=/usr/local/hadoop'>>/etc/bashrc
echo 'export HADOOP_HOME=/usr/local/hadoop'>>/etc/bashrc
echo 'export HADOOP_MAPRED_HOME=/usr/local/hadoop'>>/etc/bashrc
echo 'export HADOOP_PREFIX=/usr/local/hadoop'>>/etc/bashrc
echo 'export HADOOP_YARN_HOME=/usr/local/hadoop'>>/etc/bashrc
echo 'export HIVE_HOME=/usr/local/hive'>>/etc/bashrc
echo 'export HCAT_HOME=$HIVE_HOME/hcatalog/'>>/etc/bashrc
echo 'export JAVA_HOME=/usr/java/default'>>/etc/bashrc
echo 'export LANG=pt_br.UTF-8'>>/etc/bashrc
echo 'export LC_ALL=en_US.UTF-8'>>/etc/bashrc
echo 'export YARN_CONF_DIR=$HADOOP_PREFIX/etc/hadoop'>>/etc/bashrc
echo 'export PATH=$PATH:$JAVA_HOME/bin:/usr/local/hive/bin:/usr/local/hadoop/bin:/usr/java/default/bin:usr/local/solr-7.6.0/bin'>>/etc/bashrc
chmod 755 /etc/skel/.bashrc

users="hdfs,hive,sqoop,solr"
usernames=(${users//,/ })

USERID=1000
GROUPID=1000
groupadd -g $GROUPID hdfs

touch /etc/sudoers
chmod 555 /etc/sudoers
sed -i 's/PasswordAuthentication .*/PasswordAuthentication yes/' /etc/ssh/sshd_config

for i in "${usernames[@]}"
do
  USERNAME="${i}"
  PASSWORD="${i}"
  USERID=$((USERID+1))
  USERDIR=/home/$USERNAME

  echo "Criando usuário $USERNAME (uid=$USERID,gid=$GROUPID,dir=$USERDIR)"
  useradd -m -d ${USERDIR} -p $(openssl passwd -1 $PASSWORD) -s /bin/bash -g $GROUPID $USERNAME

  sed -i '/'"$USERNAME"' ALL=.*/d' /etc/sudoers
  echo "$USERNAME ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers 
done;

/usr/bin/ssh-keygen -A
/usr/sbin/sshd -D&

# Início do namenode
: ${HADOOP_PREFIX:=/usr/local/hadoop}
$HADOOP_PREFIX/etc/hadoop/hadoop-env.sh
/bin/rm -rf /tmp/*.pid

export HOST_NAMENODE=`hostname`
echo "Hostname atual: " $HOST_NAMENODE

if [ ! -d /hdfs/volume1/name/current ]; then
	echo "Formatando namenode"
	$HADOOP_PREFIX/bin/hdfs namenode -format
else
	echo "Namenode configurado. Pulando formatação."
fi

$HADOOP_PREFIX/sbin/hadoop-daemon.sh start namenode
$HADOOP_PREFIX/sbin/hadoop-daemon.sh start secondarynamenode

hdfs dfsadmin -safemode leave
hdfs dfs -chmod -R 777 /
touch /etc/sudoers
chmod 555 /etc/sudoers

USERNAME="hdfs"
PASSWORD="hdfs"
USERDIR=/home/$USERNAME

echo 'export HADOOP_HOME=/usr/local/hadoop'>>/etc/bashrc
echo 'export HADOOP_PREFIX=/usr/local/hadoop'>>/etc/bashrc
echo 'export HADOOP_COMMON_HOME=/usr/local/hadoop'>>/etc/bashrc
echo 'export HADOOP_HDFS_HOME=/usr/local/hadoop'>>/etc/bashrc
echo 'export HADOOP_MAPRED_HOME=/usr/local/hadoop'>>/etc/bashrc
echo 'export HADOOP_YARN_HOME=/usr/local/hadoop'>>/etc/bashrc
echo 'export HADOOP_CONF_DIR=/usr/local/hadoop/etc/hadoop'>>/etc/bashrc
echo 'export YARN_CONF_DIR=$HADOOP_PREFIX/etc/hadoop'>>/etc/bashrc
echo 'export PATH=$PATH:/usr/local/hadoop/bin'>>/etc/bashrc
echo 'export CLASSPATH=$CLASSPATH:/usr/local/hadoop/lib/*:/usr/local/hive/lib/*:.'>>/etc/bashrc
echo 'export PATH=$PATH:/usr/local/hive/bin'>>/etc/bashrc
echo 'export LANG=pt_br.UTF-8'>>/etc/bashrc
echo 'export LC_ALL=en_US.UTF-8'>>/etc/bashrc
echo 'export JAVA_HOME=/usr/java/default'>>/etc/bashrc
echo 'export PATH=$PATH:$JAVA_HOME/bin'>>/etc/bashrc

/usr/bin/ssh-keygen -A
/usr/sbin/sshd -D&
echo "Namenode ativo!"

# Início do Yarn
: ${HADOOP_PREFIX:=/usr/local/hadoop}
$HADOOP_PREFIX/etc/hadoop/hadoop-env.sh
/bin/rm -rf /tmp/*.pid

echo "Hostname atual: " `hostname`
echo "Iniciano Resource Manager"
$HADOOP_PREFIX/sbin/yarn-daemon.sh start resourcemanager

echo "Iniciando Job History Server"
$HADOOP_PREFIX/sbin/mr-jobhistory-daemon.sh start historyserver
echo "Yarn ativo!"

# Datanode
: ${HADOOP_PREFIX:=/usr/local/hadoop}
$HADOOP_PREFIX/etc/hadoop/hadoop-env.sh
/bin/rm -rf /tmp/*.pid

$HADOOP_PREFIX/sbin/hadoop-daemon.sh start datanode
$HADOOP_PREFIX/sbin/yarn-daemon.sh start nodemanager
echo "Datanode ativo!"

# Hive
while ! nc -z mysql 3306; do
  echo Aguardando MySQL...;
  sleep 1;
done;
echo MySQL ativo. Conectado!;

schematool --verbose -initSchema -dbType mysql  > /dev/null 2>&1

# Sqoop
while hdfs dfs -test -e /landing/oracle; do
  echo Aguardando Oracle...;
  sleep 1;
done;
echo Oracle ativo. Conectado!;

set -euf -o pipefail
mkdir -p /home/sqoop/teste/
echo "Criando arquivo: env_configuration.xml"
echo '<?xml version="1.0" encoding="UTF-8" standalone="no"?><configuration></configuration>'>> /home/sqoop/teste/env_configuration.xml
echo "Criando arquivo: log4j.properties"
echo ''>> /home/sqoop/teste/log4j.properties
echo "Sqoop ativo!"

# GPDB
while ! nc -z gpdb 5432; do
  echo Aguardando Greenplum...;
  sleep 1;
done;
echo Grenplum ativo. Conectado!;

# FINAL, PREVENÇÃO CONTRA FECHAMENTO
hdfs dfs -chmod -R 777 /tmp
echo "Todos os serviços do container foram subidos."
while true; do
    sleep 1;
done;
