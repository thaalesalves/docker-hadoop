FROM thaalesalves/centos:latest
LABEL Created by Thales Alves <thales@thalesalv.es>

#######################################################
### Environment
#######################################################
USER root
EXPOSE 3306 9000 12000 10000 10001 10002 1527 22 8030 8031 8032 8033 8088 10020
EXPOSE 19888 8040 8041 8042 1527 9000 50070 50090 8040 8042 50010 50020 50075

RUN echo 'alias ls="ls --block-size=M"' >> /etc/bashrc
RUN echo 'alias ll="ls -la"' >> /etc/bashrc
RUN echo 'alias l="ls -l"' >> /etc/bashrc
RUN echo 'alias la="ls -a"' >> /etc/bashrc

RUN localedef -i pt_BR -f UTF-8 pt_BR.UTF-8
ENV	LANG pt_BR.UTF-8
ENV	LANGUAGE pt_BR.UTF-8
ENV LC_CTYPE UTF-8
ENV LC_COLLATE C

RUN yum update -y
RUN yum upgrade -y
RUN yum install -y sudo openssh-clients openssh-server> /dev/null 2>&1
RUN yum install -y openssl sudo> /dev/null 2>&1
RUN yum install -y nc> /dev/null 2>&1
RUN yum install -y which> /dev/null 2>&1
RUN yum install -y telnet mc git emacs most screen curl wget nano
RUN yum install -y system-config-language
RUN yum install -y mysql postgresql
RUN mkdir /temp-files

COPY packages/jdk-8u181-linux-x64.rpm /temp-files
RUN rpm -i /temp-files/jdk-8u181-linux-x64.rpm > /dev/null 2>&1
RUN yum clean all> /dev/null 2>&1
ENV JAVA_HOME /usr/java/default
ENV PATH $PATH:$JAVA_HOME/bin

RUN groupadd docker
RUN useradd docker -d /home/docker -g docker
RUN echo "docker:docker" | chpasswd
RUN echo "root:root" | chpasswd
RUN echo '%docker ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
ENV	HOME /root

#######################################################
### Hadoop Installation
#######################################################
ADD https://downloads.thalesalv.es/docker/hadoop-image/hadoop.tar.gz /temp-files
RUN tar xvzf /temp-files/hadoop.tar.gz -C /usr/local/
ENV HADOOP_HOME /usr/local/hadoop
ENV HADOOP_PREFIX /usr/local/hadoop
ENV HADOOP_COMMON_HOME /usr/local/hadoop
ENV HADOOP_HDFS_HOME /usr/local/hadoop
ENV HADOOP_MAPRED_HOME /usr/local/hadoop
ENV HADOOP_YARN_HOME /usr/local/hadoop
ENV HADOOP_CONF_DIR /usr/local/hadoop/etc/hadoop
ENV HADOOP_CLIENT_OPTS " -Xmx2048m"
ENV YARN_CONF_DIR $HADOOP_PREFIX/etc/hadoop
ENV PATH $PATH:/usr/local/hadoop/bin
RUN	rm -rf  $HADOOP_PREFIX/etc/hadoop/*.cmd && chmod +x $HADOOP_PREFIX/etc/hadoop/*.sh

#######################################################
### SSH SERVER SETUP
#######################################################
RUN mkdir /var/run/sshd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN echo 'PasswordAuthentication yes' >> /etc/ssh/sshd_config
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

#######################################################
### Hive Installation
#######################################################
ADD https://downloads.thalesalv.es/docker/hadoop-image/apache-hive-1.2.1-bin.tar.gz /temp-files
RUN tar xvzf /temp-files/apache-hive-1.2.1-bin.tar.gz -C /usr/local/
RUN ln -s /usr/local/apache-hive-1.2.1-bin /usr/local/hive
ENV HIVE_HOME /usr/local/hive
ENV HCAT_HOME $HIVE_HOME/hcatalog/
ENV CLASSPATH $CLASSPATH:/usr/local/hadoop/lib/*:$HIVE_HOME/lib/*:.
ENV PATH $PATH:$HIVE_HOME/bin

#######################################################
### Sqoop Installation
#######################################################
ADD https://downloads.thalesalv.es/docker/hadoop-image/sqoop-1.4.7.bin__hadoop-2.6.0.tar.gz /temp-files
RUN tar xvzf /temp-files/sqoop-1.4.7.bin__hadoop-2.6.0.tar.gz -C /usr/local/
RUN mv /usr/local/sqoop-1.4.7.bin__hadoop-2.6.0 /usr/local/sqoop

ENV SQOOP_HOME /usr/local/sqoop
ENV SQOOP_CONF_DIR $SQOOP_HOME/conf
ENV PATH $SQOOP_HOME/bin:$PATH

RUN mkdir -p $SQOOP_HOME/server/lib/
RUN wget https://downloads.thalesalv.es/docker/hadoop-image/mysql-connector-java-5.1.4-bin.jar -P $SQOOP_HOME/server/lib/
COPY packages/ojdbc6.jar $SQOOP_HOME/lib/
ADD https://downloads.thalesalv.es/docker/hadoop-image/sqoop.properties $SQOOP_HOME/conf/

#######################################################
### Apache PIG Installation
#######################################################
ADD https://downloads.thalesalv.es/docker/hadoop-image/pig-0.16.0.tar.gz /temp-files
RUN tar xvzf /temp-files/pig-0.16.0.tar.gz -C /usr/local/
RUN ln -s /usr/local/pig-0.16.0 /usr/local/pig
ENV PIG_HOME /usr/local/pig
ENV PIG_CLASSPATH $HADOOP_CONF_DIR
ENV PATH $PATH:$PIG_HOME/bin

#######################################################
### Entrypoint
#######################################################
RUN rm -rf /temp-files
RUN echo "[Unit]" > /usr/lib/systemd/system/hive.service
RUN echo "Description=hive" >> /usr/lib/systemd/system/hive.service
RUN echo "[Service]" >> /usr/lib/systemd/system/hive.service
RUN echo "ExecStart=/usr/local/hive/bin/hiveserver2" >> /usr/lib/systemd/system/hive.service
RUN echo "[Install]" >> /usr/lib/systemd/system/hive.service
RUN echo "WantedBy=default.target" >> /usr/lib/systemd/system/hive.service
RUN chmod u+x /usr/lib/systemd/system/hive.service
RUN systemctl enable hive.service

ADD scripts/entrypoint.sh /entrypoint.sh
RUN chmod ug+rwx,o-wx /entrypoint.sh
RUN chown docker:root /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
